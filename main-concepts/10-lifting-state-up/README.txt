Often, several components need to reflect the same changing data.
We recommend lifting the shared state up to their closest common ancestor.

In React, sharing state is accomplished by moving it up to the closest common ancestor
of the components that need it. This is called “lifting state up”.

Lifting State Up
  https://reactjs.org/docs/lifting-state-up.html#lifting-state-up

  In React, this is usually solved by making a component “controlled”.

  Let’s recap what happens when you edit an input:
    ...

    * Inside these methods, the Calculator component asks React to re-render itself by calling this.setState()  <== setState() re-renders component (and effectively react  
      with the new input value and the current scale of the input we just edited.                                                       re-renders only the changed parts)

    * React calls the Calculator component’s render method to learn what the UI should look like.
      The values of both inputs are recomputed based on the current temperature and the active scale. The temperature conversion is performed here.

    * React calls the render methods of the individual TemperatureInput components with their new props specified by the Calculator.
      It learns what their UI should look like.

    * React calls the render method of the BoilingVerdict component, passing the temperature in Celsius as its props.

    * React DOM updates the DOM with the boiling verdict and to match the desired input values.
      The input we just edited receives its current value, and the other input is updated to the temperature after conversion.

  Every update goes through the same steps so the inputs stay in sync.

Lessons Learned
  https://reactjs.org/docs/lifting-state-up.html#lessons-learned

  There should be a single “source of truth” for any data that changes in a React application.
  Usually, the state is first added to the component that needs it for rendering.
  Then, if other components also need it, you can lift it up to their closest common ancestor.
  Instead of trying to sync the state between different components, you should rely on the top-down data flow.

  Lifting state involves writing more “boilerplate” code than two-way binding approaches,
  but as a benefit, it takes less work to find and isolate bugs.

  If something can be derived from either props or state, it probably shouldn’t be in the state.

  When you see something wrong in the UI, you can use React Developer Tools to inspect the props
  and move up the tree until you find the component responsible for updating the state. 
