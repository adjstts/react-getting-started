HTML form elements work a little bit differently from other DOM elements in React,
because form elements naturally keep some internal state.

Controlled Components
  https://reactjs.org/docs/forms.html#controlled-components

  In HTML, form elements such as <input>, <textarea>, and <select> typically maintain their own state and update it based on user input.
  In React, mutable state is typically kept in the state property of components, and only updated with setState().

  We can combine the two by making the React state be the “single source of truth”.
  Then the React component that renders a form also controls what happens in that form on subsequent user input.
  An input form element whose value is controlled by React in this way is called a “controlled component”.

  With a controlled component, every state mutation will have an associated handler function.
  This makes it straightforward to modify or validate user input.

The textarea Tag
  https://reactjs.org/docs/forms.html#the-textarea-tag

  In HTML, a <textarea> element defines its text by its children.
  In React, a <textarea> uses a value attribute instead.
  This way, a form using a <textarea> can be written very similarly to a form that uses a single-line input.

The select Tag
  https://reactjs.org/docs/forms.html#the-select-tag

  In HTML, <select> creates a drop-down list.

  React, instead of using this selected attribute, uses a value attribute on the root select tag.
  This is more convenient in a controlled component because you only need to update it in one place. 

  You can pass an array into the value attribute, allowing you to select multiple options in a select tag.

Overall, this makes it so that <input type="text">, <textarea>, and <select> all work very similarly -
they all accept a value attribute that you can use to implement a controlled component.

The file input Tag
  https://reactjs.org/docs/forms.html#the-file-input-tag

  Because its value is read-only, it is an uncontrolled component in React.
  It is discussed together with other uncontrolled components later in the documentation.

Handling Multiple Inputs
  https://reactjs.org/docs/forms.html#handling-multiple-inputs

  When you need to handle multiple controlled input elements, you can add a name attribute to each element
  and let the handler function choose what to do based on the value of event.target.name.

Controlled Input Null Value ???
  https://reactjs.org/docs/forms.html#controlled-input-null-value

  Specifying the value prop on a controlled component prevents the user from changing the input unless you desire so.
  If you’ve specified a value but the input is still editable, you may have accidentally set value to undefined or null.

Alternatives to Controlled Components
  https://reactjs.org/docs/forms.html#alternatives-to-controlled-components

  Check out uncontrolled components, an alternative technique for implementing input forms.

Fully-Fledged Solutions
  https://reactjs.org/docs/forms.html#fully-fledged-solutions

  If you’re looking for a complete solution including validation, keeping track of the visited fields,
  and handling form submission, Formik is one of the popular choices.
