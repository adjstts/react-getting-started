Why JSX?
  https://reactjs.org/docs/introducing-jsx.html#why-jsx

  **IMPORTANT**
  Instead of artificially separating technologies by putting markup and logic in separate files,
  React separates concerns with loosely coupled units called “components” that contain both.

  React doesn’t require using JSX, but most people find it helpful as a visual aid when working with UI inside the JavaScript code.

Embedding Expressions in JSX
  https://reactjs.org/docs/introducing-jsx.html#embedding-expressions-in-jsx

  You can put any valid JavaScript expression inside the curly braces in JSX.
  For example, 2 + 2, user.firstName, or formatName(user) are all valid JavaScript expressions.

JSX is an Expression Too
  https://reactjs.org/docs/introducing-jsx.html#jsx-is-an-expression-too

  After compilation, JSX expressions become regular JavaScript function calls and evaluate to JavaScript objects.

Specifying Attributes with JSX
  https://reactjs.org/docs/introducing-jsx.html#specifying-attributes-with-jsx

  Don’t put quotes around curly braces when embedding a JavaScript expression in an attribute.
  You should either use quotes (for string values) or curly braces (for expressions), but not both in the same attribute.  

  **IMPORTANT**
  Since JSX is closer to JavaScript than to HTML, React DOM uses camelCase property naming convention instead of HTML attribute names.
  For example, class becomes className in JSX, and tabindex becomes tabIndex.

JSX Prevents Injection Attacks
  https://reactjs.org/docs/introducing-jsx.html#jsx-prevents-injection-attacks

  By default, React DOM escapes any values embedded in JSX before rendering them.
  Thus it ensures that you can never inject anything that’s not explicitly written in your application.

JSX Represents Objects
  https://reactjs.org/docs/introducing-jsx.html#jsx-represents-objects

  Babel compiles JSX down to React.createElement() calls which create objects called “React elements”.

