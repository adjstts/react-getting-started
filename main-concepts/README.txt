https://reactjs.org/docs/hello-world.html
Every chapter in this guide builds on the knowledge introduced in earlier chapters.
You can learn most of React by reading the “Main Concepts” guide chapters in the order they appear in the sidebar.

prerequisites
  https://reactjs.org/docs/hello-world.html#knowledge-level-assumptions
  https://gist.github.com/gaearon/683e676101005de0add59e8bb345340c
