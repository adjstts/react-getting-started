/* Сделал пока без чекбокса */

const DATA = [
  {category: "Sporting Goods", price: "$49.99", stocked: true, name: "Football"},
  {category: "Sporting Goods", price: "$9.99", stocked: true, name: "Baseball"},
  {category: "Sporting Goods", price: "$29.99", stocked: false, name: "Basketball"},
  {category: "Electronics", price: "$99.99", stocked: true, name: "iPod Touch"},
  {category: "Electronics", price: "$399.99", stocked: false, name: "iPhone 5"},
  {category: "Electronics", price: "$199.99", stocked: true, name: "Nexus 7"}
];

class FilterableProductTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = { searchString: "" };

    this.handleSearchStringChange = this.handleSearchStringChange.bind(this);
  }

  handleSearchStringChange(newSearchString) {
    // https://reactjs.org/docs/state-and-lifecycle.html#state-updates-are-merged
    this.setState({ searchString: newSearchString });
    console.log("new search string value: " + newSearchString);
  }

  filterByName(data) {
    return data.filter(item => item.name.toLowerCase()
      .includes(this.state.searchString.toLowerCase()));
  }

  render() {
    return (
      <div>
        <SearchBar searchString={this.state.searchString}
          onSearchStringChange={this.handleSearchStringChange} />
        <ProductTable data={this.filterByName(this.props.data)} />
      </div>
    );
  }
}

class SearchBar extends React.Component {
  constructor(props) {
    super(props);

    this.handleSearchStringChange = this.handleSearchStringChange.bind(this);
  }

  handleSearchStringChange(e) {
    this.props.onSearchStringChange(e.target.value);
  }

  render() {
    return (
      <div>
        <input value={this.props.searchString} onChange={this.handleSearchStringChange} /> 
      </div>
    );
  }
}

class ProductTable extends React.Component {
  sortByCategoryAndName(firstItem, secondItem) {
    let categoryDifference = firstItem.category.localeCompare(secondItem.category);

    return categoryDifference !== 0
      ? categoryDifference
      : firstItem.name.localeCompare(secondItem.name);
  }

  createRows(data) {
    let rows = [];
    let currentCategory;

    data.forEach(item => {
      if (item.category !== currentCategory) {
        rows.push(
          // https://reactjs.org/docs/lists-and-keys.html#keys
          <ProductCategoryRow name={item.category} key={item.category} />
        );

        currentCategory = item.category;
      }

      rows.push(
        // https://reactjs.org/docs/lists-and-keys.html#keys
        <ProductRow name={item.name} price={item.price} key={item.name} />
      );
    });

    return rows;
  }

  render() {
    return(
      <table>
        <thead><tr><th>Name</th><th>Price</th></tr></thead>
        <tbody>{this.createRows(this.props.data.sort(this.sortByCategoryAndName))}</tbody>
      </table>
    );
  }
}

function ProductCategoryRow(props) {
  return (
    // https://reactjs.org/docs/introducing-jsx.html#specifying-attributes-with-jsx
    // Warning:
    // Since JSX is closer to JavaScript than to HTML, React DOM uses camelCase property naming convention instead of HTML attribute names.
    // For example, class becomes className in JSX, and tabindex becomes tabIndex.
    <tr><td colSpan="2"><b>{props.name}</b></td></tr>
  );
}

function ProductRow(props) {
  return (
    <tr><td>{props.name}</td><td>{props.price}</td></tr>
  );
}

ReactDOM.render(
  <FilterableProductTable data={DATA} />,
  document.getElementById("root")
);
