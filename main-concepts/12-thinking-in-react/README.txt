Step 2: Build A Static Version in React
  https://reactjs.org/docs/thinking-in-react.html#step-2-build-a-static-version-in-react

  Now that you have your component hierarchy, it’s time to implement your app. The easiest
  way is to build a version that takes your data model and renders the UI but has no
  interactivity. It’s best to decouple these processes because building a static version requires
  a lot of typing and no thinking, and adding interactivity requires a lot of thinking and not a
  lot of typing.

  To build a static version of your app that renders your data model, you’ll want to build
  components that reuse other components and pass data using props. props are a way of
  passing data from parent to child.

  **IMPORTANT**
  If you’re familiar with the concept of state, don’t use state at all to build this static version.
  State is reserved only for interactivity, that is, data that changes over time. Since this is a
  static version of the app, you don’t need it.

Step 4: Identify Where Your State Should Live
  https://reactjs.org/docs/thinking-in-react.html#step-4-identify-where-your-state-should-live

  Remember: React is all about one-way data flow down the component hierarchy.
  It may not be immediately clear which component should own what state.
  This is often the most challenging part for newcomers to understand.


  ..там вообще нечего из главы вырезать, она вся полезная

  React uses ONE-WAY binding
