Components let you split the UI into independent, reusable pieces, and think about each piece in isolation.

Conceptually, components are like JavaScript functions.
They accept arbitrary inputs (called “props”) and return React elements describing what should appear on the screen.

Function and Class Components
  https://reactjs.org/docs/components-and-props.html#function-and-class-components

  The simplest way to define a component is to write a JavaScript function.
  You can also use an ES6 class to define a component.

  Classes have some additional features (see https://reactjs.org/docs/state-and-lifecycle.html)

Rendering a Component
  https://reactjs.org/docs/components-and-props.html#rendering-a-component

  When React sees an element representing a user-defined component,
  it passes JSX attributes to this component as a single object. We call this object “props”.

  **IMPORTANT**
  Always start component names with a capital letter.
  React treats components starting with lowercase letters as DOM tags.
  For example, <div /> represents an HTML div tag,
  but <Welcome /> represents a component and requires Welcome to be in scope.

Composing Components
  https://reactjs.org/docs/components-and-props.html#composing-components

  Components can refer to other components in their output.
  This lets us use the same component abstraction for any level of detail.

  Typically, new React apps have a single App component at the very top.
  However, if you integrate React into an existing app, you might start bottom-up
  with a small component like Button and gradually work your way to the top of the view hierarchy.

Extracting Components
  https://reactjs.org/docs/components-and-props.html#extracting-components

  Don’t be afraid to split components into smaller components.

  We recommend naming props from the component’s own point of view
  rather than the context in which it is being used.

  Extracting components might seem like grunt work at first, but having a palette of reusable components pays off in larger apps.
  A good rule of thumb is that if a part of your UI is used several times (Button, Panel, Avatar),
  or is complex enough on its own (App, FeedStory, Comment), it is a good candidate to be a reusable component.

Props are Read-Only
  https://reactjs.org/docs/components-and-props.html#props-are-read-only

  Whether you declare a component as a function or a class, it must never modify its own props.

  React is pretty flexible but it has a single strict rule:
  All React components must act like pure functions with respect to their props.

  See https://reactjs.org/docs/state-and-lifecycle.html for the concept of "state".
  State allows React components to change their output over time in response to
  user actions, network responses, and anything else, without violating this rule.
