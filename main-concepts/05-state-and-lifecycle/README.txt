https://reactjs.org/docs/state-and-lifecycle.html

State is similar to props, but it is private and fully controlled by the component.

We mentioned before that components defined as classes have some additional features.
Local state is exactly that: a feature available only to classes.

Adding Lifecycle Methods to a Class
  https://reactjs.org/docs/state-and-lifecycle.html#adding-lifecycle-methods-to-a-class

  In applications with many components, it’s very important to free up resourcestaken by the components when they are destroyed.

  We can declare special methods on the component class to run some code when a component "mounts" and "unmounts".
  These methods are called “lifecycle methods”.
  The componentDidMount() method runs after the component output has been rendered to the DOM.
  The componentWillUnmount() is caled by React whenever the component is removed from the DOM.

  While this.props is set up by React itself
  and this.state has a special meaning,                                                        <== **IMPORTANT** state is reactive
  you are free to add additional fields to the class manually
  if you need to store something that doesn’t participate in the data flow (like a timer ID).

Using State Correctly
  https://reactjs.org/docs/state-and-lifecycle.html#using-state-correctly

  Do Not Modify State Directly
    https://reactjs.org/docs/state-and-lifecycle.html#do-not-modify-state-directly

    The only place where you can assign this.state is the constructor.
    Use setState() everywhere else.

  State Updates May Be Asynchronous [#ff0 TODO перечитать эту главу еще раз, к ней есть небольшой вопрос]
    https://reactjs.org/docs/state-and-lifecycle.html#state-updates-may-be-asynchronous

    React may batch multiple setState() calls into a single update for performance.

    Because this.props and this.state may be updated asynchronously,
    you should not rely on their values for calculating the next state.
    Use a second form of setState() that accepts a function rather than an object.
    That function will receive the previous state as the first argument,
    and the props at the time the update is applied as the second argument.

  State Updates are Merged
    https://reactjs.org/docs/state-and-lifecycle.html#state-updates-are-merged

    When you call setState(), React merges the object you provide into the current state.

    The merging is shallow, so you can update several state variables independently with separate setState() calls.

The Data Flows Down
  https://reactjs.org/docs/state-and-lifecycle.html#the-data-flows-down

  Neither parent nor child components can know if a certain component is stateful or stateless,
  and they shouldn’t care whether it is defined as a function or a class.

  This is why state is often called local or encapsulated. It is not accessible to any component
  other than the one that owns and sets it.

  A component may choose to pass its state down as props to its child components.

  This is commonly called a “top-down” or “unidirectional” data flow.
  Any state is always owned by some specific component, and any data or UI derived from that state
  can only affect components “below” them in the tree.

  In React apps, whether a component is stateful or stateless is considered an implementation detail of the component
  that may change over time. You can use stateless components inside stateful components, and vice versa.

  