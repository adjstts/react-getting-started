Keys
  https://reactjs.org/docs/lists-and-keys.html#keys

  Keys help React identify which items have changed, are added, or are removed.
  Keys should be given to the elements inside the array to give the elements a stable identity.

  Most often you would use IDs from your data as keys.
  When you don’t have stable IDs for rendered items, you may use the item index as a key as a last resort.
  We don’t recommend using indexes for keys if the order of items may change.

Extracting Components with Keys
  https://reactjs.org/docs/lists-and-keys.html#extracting-components-with-keys

  Keys only make sense in the context of the surrounding array.
  For example, if you extract a ListItem component, you should keep the key
  on the <ListItem /> elements in the array rather than on the <li> element in the ListItem itself.

Keys Must Only Be Unique Among Siblings
  https://reactjs.org/docs/lists-and-keys.html#keys-must-only-be-unique-among-siblings

  Keys used within arrays should be unique among their siblings.
  However they don’t need to be globally unique.

  Keys serve as a hint to React but they don’t get passed to your components.

  key is not added to props
