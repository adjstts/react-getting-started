React has a powerful composition model, and we recommend using
composition instead of inheritance to reuse code between components.

Containment
  https://reactjs.org/docs/composition-vs-inheritance.html#containment

  Some components don’t know their children ahead of time. This is especially common for
  components like Sidebar or Dialog that represent generic “boxes”.

  We recommend that such components use the special 'children' prop to pass children
  elements directly into their output.

  This lets other components pass arbitrary children to them by nesting the JSX.

  While this is less common, sometimes you might need multiple “holes” in a component.
  In such cases you may come up with your own convention instead of using children

Specialization
  https://reactjs.org/docs/composition-vs-inheritance.html#specialization

  Sometimes we think about components as being “special cases” of other components.
  For example, we might say that a WelcomeDialog is a special case of Dialog.

  In React, this is also achieved by composition, where a more “specific” component renders a
  more “generic” one and configures it with props

So What About Inheritance?
  https://reactjs.org/docs/composition-vs-inheritance.html#so-what-about-inheritance

  At Facebook, we use React in thousands of components, and we haven’t found any use
  cases where we would recommend creating component inheritance hierarchies.

  If you want to reuse non-UI functionality between components, we suggest extracting it into
  a separate JavaScript module. The components may import it and use that function, object,
  or a class, without extending it.
