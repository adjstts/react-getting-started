Handling events with React elements is very similar to handling events on DOM elements.
There are some syntactic differences:
  * React events are named using camelCase, rather than lowercase.
  * With JSX you pass a function as the event handler, rather than a string.

You cannot return false to prevent default behavior in React.
You must call preventDefault explicitly.

React defines synthetic events according to the W3C spec,
so you don’t need to worry about cross-browser compatibility.

You have to be careful about the meaning of this in JSX callbacks. In JavaScript, class methods are not bound by default.
This is not React-specific behavior; it is a part of how functions work in JavaScript.
Generally, if you refer to a method without () after it, such as onClick={this.handleClick}, you should bind that method.

If calling bind annoys you, there are two ways you can get around this.
If you are using the experimental public class fields syntax, you can use class fields to correctly bind callbacks.
This syntax is enabled by default in Create React App.

If you aren’t using class fields syntax, you can use an arrow function in the callback.
The problem with this syntax is that a different callback is created each time the component renders.
We generally recommend binding in the constructor or using the class fields syntax, to avoid this sort of performance problem.

