Elements are the smallest building blocks of React apps.

Unlike browser DOM elements, React elements are plain objects, and are cheap to create.
React DOM takes care of updating the DOM to match the React elements.

Elements are what components are “made of”.

Rendering an Element into the DOM
  https://reactjs.org/docs/rendering-elements.html#rendering-an-element-into-the-dom

  Applications built with just React usually have a single root DOM node.
  If you are integrating React into an existing app, you may have as many isolated root DOM nodes as you like.

  To render a React element into a root DOM node, pass both to ReactDOM.render()

Updating the Rendered Element
  https://reactjs.org/docs/rendering-elements.html#updating-the-rendered-element

  React elements are immutable. Once you create an element, you can’t change its children or attributes.

  In practice, most React apps only call ReactDOM.render() once.

React Only Updates What’s Necessary
  https://reactjs.org/docs/rendering-elements.html#react-only-updates-whats-necessary

  React DOM compares the element and its children to the previous one,
  and only applies the DOM updates necessary to bring the DOM to the desired state.

  Even though we create an element describing the whole UI,
  only the text node whose contents has changed gets updated by React DOM.

  Thinking about how the UI should look at any given moment rather than how to change it over time
  eliminates a whole class of bugs.
