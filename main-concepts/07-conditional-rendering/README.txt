Conditional rendering in React works the same way conditions work in JavaScript.
Use JavaScript operators like if or the conditional operator to create elements
representing the current state, and let React update the UI to match them.

Element Variables
  https://reactjs.org/docs/conditional-rendering.html#element-variables

  You can use variables to store elements. This can help you conditionally render
  a part of the component while the rest of the output doesn’t change.

Inline If with Logical && Operator (потому что в js && и || - селекторы)
  https://reactjs.org/docs/conditional-rendering.html#inline-if-with-logical--operator

  You may embed any expressions in JSX by wrapping them in curly braces.
  This includes the JavaScript logical && operator. It can be handy for conditionally including an element

Inline If-Else with Conditional Operator
  https://reactjs.org/docs/conditional-rendering.html#inline-if-else-with-conditional-operator

  Another method for conditionally rendering elements inline is to use
  the JavaScript conditional operator condition ? true : false.

  Also remember that whenever conditions become too complex, it might be a good time to extract a component.

Preventing Component from Rendering
  https://reactjs.org/docs/conditional-rendering.html#preventing-component-from-rendering

  In rare cases you might want a component to hide itself even though it was rendered by another component.
  To do this return null instead of its render output.

  Returning null from a component’s render method does not affect the firing of the component’s lifecycle methods.
  For instance componentDidUpdate will still be called.
