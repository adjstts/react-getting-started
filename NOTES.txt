npm vs yarn
  npm > yarn:
    * yarn uses Facebook’s npm registry mirror.
  npm == yarn:
    * npm с пятой версии начал вест package-lock.json, то есть yarn.lock теперь не аргумент.
  npm < yarn:
    * yarn ставит пакеты параллельно, Димас говорит что на практике получается заметно быстрее чем npm.
    * yarn локально кэширует поставленные пакеты в ~/.yarn-cache (что-то в духе ~/.m2/repository).
  ссылки
    https://blog.risingstack.com/yarn-vs-npm-node-js-package-managers/
  примечания
    yarn вроде как является клиентом npm, поэтому возможно правильнее будет не npm vs yarn, a npm-cli vs yarn.

webpack vs gulp/grunt
  gulp и grunt - build tools / task runners. функциональность у них пимерно одинаковая, но gulp сильно проще в использовании.
  Comparing Grunt vs Gulp, the core difference is that where GruntJS uses configuration objects to define tasks declaratively,
  Gulp defines them as JavaScript functions.

  webpack - static module bundler. он часто используется вместе, например, с gulp, но в последнее время стал так много уметь, что можно обойтись только webpack.
  The idea of dependency graph in Webpack ensures easier splitting of code, control over assets processing, and elimination of dead assets.

  ссылки
    https://da-14.com/blog/gulp-vs-grunt-vs-webpack-comparison-build-tools-task-runners
    https://trends.google.com/trends/explore?date=all&q=gulp,grunt,webpack

commonsjs vs requirejs vs amd vs umd


creating a toolchain from scratch
  https://reactjs.org/docs/create-a-new-react-app.html#creating-a-toolchain-from-scratch

  A JavaScript build toolchain typically consists of:
    * A package manager, such as Yarn or npm. It lets you take advantage of a vast ecosystem of third-party packages, and easily install or update them.
    * A bundler, such as webpack or Parcel. It lets you write modular code and bundle it together into small packages to optimize load time.
    * A compiler such as Babel. It lets you write modern JavaScript code that still works in older browsers.

  If you prefer to set up your own JavaScript toolchain from scratch,
  check out this guide https://blog.usejournal.com/creating-a-react-app-from-scratch-f3c693b84658.

  Don’t forget to ensure your custom toolchain is correctly set up for production.
