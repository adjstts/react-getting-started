основной туториал
  https://reactjs.org/docs/create-a-new-react-app.html#create-react-app

workflow
  npx create-react-app my-app
  cd my-app
  npm start

other toolchains
  Next.js
    a popular and lightweight framework for static and server‑rendered applications built with React.
  Gatsby
    the best way to create static websites with React.
    It lets you use React components, but outputs pre-rendered HTML and CSS to guarantee the fastest load time.
  Neutrino
    combines the power of webpack with the simplicity of presets, and includes a preset for React apps and React components.
  nwb
    particularly great for publishing React components for npm. It can be used for creating React apps, too.
  Parcel
    a fast, zero configuration web application bundler that works with React.
  Razzle
    a server-rendering framework that doesn’t require any configuration, but offers more flexibility than Next.js.