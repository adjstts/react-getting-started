// In case you're wondering about the path module being imported at the top,
// it is a core Node.js module that gets used to manipulate file paths.
const path = require('path');

// https://stackoverflow.com/questions/32155154/webpack-config-how-to-just-copy-the-index-html-to-the-dist-folder
// https://www.npmjs.com/package/copy-webpack-plugin
// TODO попробовать HtmlWebpackPlugin для генерации index.html
const CopyWebpackPlugin = require('copy-webpack-plugin');

const SRC = 'src';
const DEST = 'output';

module.exports = {
  entry: path.resolve(__dirname, SRC, 'index.jsx'),  // https://webpack.js.org/concepts/#entry
  output: {                                          // https://webpack.js.org/concepts/#output
    path: path.resolve(__dirname, DEST),
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    rules: [  // https://webpack.js.org/concepts/#loaders
      {
        test: /\.jsx/,
        use: {
          loader: 'babel-loader',
          options: { presets: ['react', 'es2015'] }
        }        
      },
      {
        test: /\.scss/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [
    // https://webpack.js.org/concepts/#plugins
    // Since you can use a plugin multiple times in a config for different purposes,
    // you need to create an instance of it by calling it with the new operator.
    new CopyWebpackPlugin([
      // так неправильно:
      // { from: `${SRC}/index.html`, to:`${DEST}/index.html` }
      // так правильно:
      // { from: `${SRC}/index.html`, to:"index.html" }
      // а так красивее всего:
      `${SRC}/index.html`
    ])
  ]

  // https://webpack.js.org/concepts/#mode
};

// https://webpack.js.org/concepts/#browser-compatibility