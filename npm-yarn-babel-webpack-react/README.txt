туториал, взятый за основу
    https://medium.com/front-end-weekly/what-are-npm-yarn-babel-and-webpack-and-how-to-properly-use-them-d835a758f987

примечания
    из коробки этот туториал не работает, при запуске yarn run build, возникает ошибка:

    [0] ./src/index.jsx 3.36 KiB {0} [not cacheable] [built] [failed] [1 error]

    ERROR in ./src/index.jsx
    Module build failed (from ./node_modules/babel-loader/lib/index.js):
    Error: Cannot find module '@babel/core'
     babel-loader@8 requires Babel 7.x (the package '@babel/core'). If you'd like to use Babel 6.x ('babel-core'), you should install 'babel-loader@7'.
        at Function.Module._resolveFilename (module.js:476:15)
        at Function.Module._load (module.js:424:25)
        at Module.require (module.js:504:17)
        at require (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/v8-compile-cache/v8-compile-cache.js:159:20)
        at Object.<anonymous> (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/babel-loader/lib/index.js:10:11)
        at Module._compile (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/v8-compile-cache/v8-compile-cache.js:178:30)
        at Object.Module._extensions..js (module.js:586:10)
        at Module.load (module.js:494:32)
        at tryModuleLoad (module.js:453:12)
        at Function.Module._load (module.js:445:3)
        at Module.require (module.js:504:17)
        at require (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/v8-compile-cache/v8-compile-cache.js:159:20)
        at loadLoader (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/loader-runner/lib/loadLoader.js:18:17)
        at iteratePitchingLoaders (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/loader-runner/lib/LoaderRunner.js:169:2)
        at runLoaders (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/loader-runner/lib/LoaderRunner.js:365:2)
        at NormalModule.doBuild (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/webpack/lib/NormalModule.js:280:3)
        at NormalModule.build (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/webpack/lib/NormalModule.js:427:15)
        at Compilation.buildModule (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/webpack/lib/Compilation.js:633:10)
        at moduleFactory.create (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/webpack/lib/Compilation.js:1019:12)
        at factory (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/webpack/lib/NormalModuleFactory.js:405:6)
        at hooks.afterResolve.callAsync (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/webpack/lib/NormalModuleFactory.js:155:13)
        at AsyncSeriesWaterfallHook.eval [as callAsync] (eval at create (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/tapable/lib/HookCodeFactory.j
    s:32:10), <anonymous>:6:1)
        at AsyncSeriesWaterfallHook.lazyCompileHook (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/tapable/lib/Hook.js:154:20)
        at resolver (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/webpack/lib/NormalModuleFactory.js:138:29)
        at process.nextTick (/home/adjstts/Sandbox/react/getting-started/npm-yarn-babel-webpack-react/node_modules/webpack/lib/NormalModuleFactory.js:342:9)
        at _combinedTickCallback (internal/process/next_tick.js:73:7)
        at process._tickCallback (internal/process/next_tick.js:104:9)
    error Command failed with exit code 2.

    версии зависимостей лучше указывать сразу явно, или по мере необходимости использовать yarn upgrade [package]@[version]
    в моём случае, babel-core@7 был еще beta, поэтому поапгрейдил babel-loader@8 до babel-loader@7
