import React from 'react';
import ReactDOM from 'react-dom';

import HelloWorld from './components/HelloWorld';

// https://webpack.js.org/concepts/#loaders
// Note that the ability to import any type of module, e.g. .css files, is a feature specific to webpack
// and may not be supported by other bundlers or task runners. We feel this extension of the language is
// warranted as it allows developers to build a more accurate dependency graph. 
import './styles/app.scss';

ReactDOM.render(
  <HelloWorld />,
  document.getElementById('app')
);
